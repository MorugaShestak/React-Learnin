import React, {useRef, useState} from 'react';
import "./styles/App.css"
import Counter from "./components/Counter";
import PostItem from "./components/PostItem";
import PostList from "./components/PostList";
import MyButton from "./components/UI/button/MyButton";
import MyInput from "./components/UI/input/MyInput";
import PostForm from "./components/PostForm";
import MySelect from "./components/UI/select/MySelect";


function App() {

    const[posts, setPosts] = useState(
        [

            {
                id: 0,
                title: "JS",
                body: "Desc",
            },

            {
                id: 1,
                title: "Python",
                body: "Desc2",
            }

        ]
    )
    const [title, setTitle] = useState('')
    const [body, setBody] = useState('')


const createPost = (newPost) => {
        setPosts([...posts, newPost])
}

const removePost = (post) => {
        setPosts(posts.filter(p => p.id !== post.id))
}

  return (

    <div className="App">
        <PostForm create={createPost}/>
        <hr style={{margin: 15}}/>
            <MySelect defaultValue="Sort by" options={[
                {
                    name: "Name",
                    value: "name",
                },
                {
                    name: "Desc",
                    value: "Desc"
                }
            ]} />
        {
            posts.length !== 0
        ? <PostList remove={removePost} posts={posts} title="Yee"/>
                : <h1 style={{textAlign: 'center'}}> Нет постов </h1>
        }

    </div>
  );
}

export default App;
