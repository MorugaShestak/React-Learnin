import React, {useState} from 'react';
import MyInput from "./UI/input/MyInput";
import MyButton from "./UI/button/MyButton";

const PostForm = ({create}) => {

    const [post, setPost] = useState({title: '', body: ''})

    function addNewPost(e) {
        e.preventDefault()

        const newPost = {
            ...post, id: Date.now()
        }
        create(newPost)
        setPost({title: '', body: ''})
    }

    return (
        <form>
            {/* Управляемые элемент*/}
            <MyInput value={post.title} onChange={e => setPost({...post, title: e.target.value})}>Name</MyInput>
            {/* Неуправляемый элемент*/}
            <MyInput value={post.body} onChange={e => setPost({...post, body: e.target.value})}>Desc</MyInput>
            <MyButton type="submit" onClick={addNewPost}>Create</MyButton>
        </form>
    );
};

export default PostForm;