import React from 'react';
import classes from "./MyInput.module.css";

const MyInput = ({children, ...props}) => {
    return (
        <input className={classes.myInput} placeholder={children} {...props}>

        </input>
    );
};

export default MyInput;