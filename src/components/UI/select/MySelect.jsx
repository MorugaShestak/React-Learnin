import React from 'react';
import classes from "./MySelect.module.css"

const MySelect = ({options, defaultValue}) => {
    return (
        <select className={classes.mySelect}>
            <option disabled value="">{defaultValue}</option>
            {options.map((p) => <option value={p.value}>{p.name}</option>)}
        </select>
    );
};

export default MySelect;